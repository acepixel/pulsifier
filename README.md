
# Pulsifier
Pulsifier is a modification client that alters Roblox to prevent forced update pushes. Its motive is to assist UWP exploiters, as there is currently no other way to exploit the platform.

## Installation

- [📥 Install python](https://python.org/downloads)
Or run install_python.bat if you are too lazy to do this.
- [📥 Download the latest release](https://github.com/acep1xel/pulsifier/releases/new)
- ✨ Extract the zip file
- 👆 Open pulsifier.py
make sure developer mode is on, dw if it isnt it will take you to the settings page to turn it on.
## Supported By

Pulsifier is supported by these major executors, officially, or even more, unofficaly.
- Fluxus 
- Sirhurt


## Screenshots

- Opening![App Screenshot](https://media.discordapp.net/attachments/957403482539823144/1168923686397554780/image.png?ex=65538804&is=65411304&hm=306a1eea63bd76e4eb2c9d69652350a1a6978305fc2ed49bb013afc893ffcbb4&=&width=1232&height=387)
Patching roblox game files :
- Downloading Packages![ok](https://media.discordapp.net/attachments/957403482539823144/1168921777557221467/image.png?ex=6553863d&is=6541113d&hm=3700c2cb7a5ff1f90f8d55611dbde692d631a4bd0a1a5e2a86777aea2ebf2d03&=&width=970&height=206)
- Applying security measures![ok](https://media.discordapp.net/attachments/957403482539823144/1168921746469028063/image.png?ex=65538635&is=65411135&hm=34cb84b43b237542422e38cb9deed5cad0892d73ba791c7bd37c1675ad9e2f8d&=&width=1302&height=290)
- Patching roblox updates prompts![ok]()
- Optimizing performance![ok](https://media.discordapp.net/attachments/957403482539823144/1168921777305559116/image.png?ex=6553863d&is=6541113d&hm=5491e9eb242a508ce59de7a957d33ebd68243f06ace682fc1f7e01646b193d67&=&width=1353&height=270)
- 
## Common questions

- #### Developer mode not enabled

Just enable it, easy. When its not enabled, it opens the settings and you just have to toggle on.

- #### Pulsifier.exe not found

**IT SHOULD BE IN THE SAME FOLDER AS PULSIFIER.PY**   
Your antivirus might have deleted the exe due to obvious reasons, disable any of those and download it again.



## Support

For support, join 


## Badges

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)


## Authors

- [acepixel](https://gitea.com/acepixel)

